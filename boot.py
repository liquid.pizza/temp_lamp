from machine import Pin, unique_id, I2C
import time
import network
import ubinascii
from umqtt.simple import MQTTClient

import neopixel

import config
CLIENT_ID = ubinascii.hexlify(unique_id())

np = neopixel.NeoPixel(Pin(config.NEOPX_PIN), config.NEOPX_LEN)

# setup wifi
wlan = network.WLAN(network.STA_IF) # create station interface
wlan.active(True)       # activate the interface
wlan.connect(config.WIFI_SSID, config.WIFI_PASS) # connect to an AP

print("Connecting to {}".format(config.WIFI_SSID))

# connect to the wifi
while not wlan.isconnected():
    pass

print("Connected with {}".format(config.WIFI_SSID))

print("Connecting with MQTT broker @ {}".format(config.SERVER_URL))

# create MQTT client
c = MQTTClient(CLIENT_ID, config.SERVER_URL)

def connect_to_server():
    # wait for a connection
    while 1:
        try:
            if not c.connect():
                break
        except OSError:
            print("Server not reachable.")
            print("Waiting {}s before trying to re connecting to {}".format(config.RECONNECT_TIME_S, config.SERVER_URL))
            time.sleep(config.RECONNECT_TIME_S)
            return False

    print("Connected with MQTT broker @ {}".format(config.SERVER_URL))
    return True

def generateTopic(sensor_key):
    # topic is /<BASE_KEY>/<ROOM_KEY>/<SENSOR_KEY>
    return "/{}/{}/{}".format(config.BASE_KEY, config.ROOM_KEY, sensor_key)

'''
This function takes a value x and two fixpoints p0=(x0, y0) and p1=(x1, y1) as input.
Where y0=(r0, g0, b0) and y1=(r1, g1, b1) are RGB colors.

It then computes a linear function for each color channel (r(x), g(x), b(x)) and computes the value for the given input x. 

The return value is a RGB color.
'''
def calc_val(x, p0=config.P0, p1=config.P1):
    x0, c0 = p0
    x1, c1 = p1

    return [foo(x, (x0, c0[i]), (x1, c1[i])) for i,_ in enumerate(c0)]

'''
Helper function for linear interpolation through two fixpoints p0=(x0, y0) and p1=(x1, y1).

It returns the function value for the input x within this function. 
'''
def foo(x, p0, p1):
    x0, y0 = p0
    x1, y1 = p1
    return int(bound((y1-y0)/(x1-x0)*(x-x0)+y0))

def test_colors(n=1, d=1):
    for i in range(n):
        for t in range(0, 4000):
            col = calc_val(t/100)
            for p in range(np.n):
                np[p] = col
            print(t/100, col)
            np.write()
            time.sleep(d)

'''
This fucntion takes a value x and a lower and an upper bound.

It ensures, that lower <= x <= upper.
'''
def bound(x, lower=0, upper=255):
    return min(255, max(0, x))

def on_message(t, m):
    p = m.decode()
    col = calc_val(float(p))
    print(p, col)
    for i in range(np.n):
        np[i] = col
    np.write()

# main loop
if (connect_to_server()):
    c.set_callback(on_message)
    c.subscribe(generateTopic(config.TEMP_KEY))

    while 1:
        c.wait_msg()
